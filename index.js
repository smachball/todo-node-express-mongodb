// DEPENDENCIES
var express = require("express");
var app = express();
var bodyParser = require("body-parser");

// DATABASE CONNECTION
var mongo = require("mongodb");
var MongoClient = mongo.MongoClient;
let db;

// ENV FILE
const config = require("dotenv").config();
const { DB_HOST, DB_NAME, SERVER_PORT } = config.parsed;

// CONNECT TO MANGODB SERVER
MongoClient.connect(DB_HOST, function (err, client) {
  if (err) {
    console.log("MONGODB => " + err);
  } else {
    console.log("MONGODB => Connected successfully to server");

    db = client.db(DB_NAME);
    if (db) {
      console.log("MONGODB => Connected successfully to db : " + DB_NAME);
    } else {
      console.log("MONGODB => Connection error to db : " + DB_NAME);
    }
  }
});

// JUST A TEST FOR APPLICATION READY
app.get("/", function (req, res) {
  res.setHeader("Content-Type", "application/json");
  res.send(
    JSON.stringify({
      status: true,
      message: "TODO Application Running !",
    })
  );
});

//============= CRUD TODO =============//

// LIST
app.get("/list", function (req, res) {
  switch (req.query.status) {
    case "open":
      db.collection("todo")
        .find({ status: "todo" })
        .toArray(function (err, result) {
          res.setHeader("Content-Type", "application/json");
          res.send(
            JSON.stringify({
              status: true,
              result,
            })
          );
          db.close();
        });
      break;
    case "finish":
      db.collection("todo")
        .find({ status: "finish" })
        .toArray(function (err, result) {
          res.setHeader("Content-Type", "application/json");
          res.send(
            JSON.stringify({
              status: true,
              result,
            })
          );
          db.close();
        });
      break;
    default:
      db.collection("todo")
        .find({})
        .toArray(function (err, result) {
          res.setHeader("Content-Type", "application/json");
          res.send(
            JSON.stringify({
              status: true,
              result,
            })
          );
          db.close();
        });
      break;
  }
});

// SHOW
app.get("/show/:id", function (req, res) {
  res.setHeader("Content-Type", "application/json");
  let param_id;
  try {
    param_id = new mongo.ObjectID(req.params.id);
  } catch (e) {
    res.send(
      JSON.stringify({
        status: false,
        message: e.message,
      })
    );
  }

  db.collection("todo").findOne({ _id: param_id }, function (err, result) {
    res.send(
      JSON.stringify({
        status: true,
        result,
      })
    );

    db.close();
  });
});

// CREATE
app.get("/create", function (req, res) {
  res.setHeader("Content-Type", "application/json");

  const { title } = req.query;

  if (!title) {
    res.status(400).send(
      JSON.stringify({
        status: false,
        message: "title.required",
      })
    );
  } else {
    db.collection("todo").insertOne(
      { title: title, status: "todo" },
      function (err, result) {
        let param_id = new mongo.ObjectID(result.insertedId);

        db.collection("todo").findOne(
          { _id: param_id },
          function (err, result) {
            res.send(
              JSON.stringify({
                status: true,
                message: "TODO-ISSUE opened successfully",
                result,
              })
            );
            db.close();
          }
        );
      }
    );
  }
});

// UPDATE
app.get("/update/:id", function (req, res) {
  res.setHeader("Content-Type", "application/json");
  let param_id;
  try {
    param_id = new mongo.ObjectID(req.params.id);
  } catch (e) {
    res.send(
      JSON.stringify({
        status: false,
        message: e.message,
      })
    );
  }
  db.collection("todo").findOne({ _id: param_id }, function (err, result) {
    const status = req.query.status || result.status;
    const title = req.query.title || result.title;
    db.collection("todo").updateOne(
      { _id: param_id },
      { $set: { status: status, title: title } },
      function (err, result) {
        db.collection("todo").findOne(
          { _id: param_id },
          function (err, result) {
            res.send(
              JSON.stringify({
                status: true,
                message: "TODO-ISSUE updated successfully",
                result,
              })
            );
            db.close();
          }
        );
      }
    );
  });
});

// OPEN
app.get("/open/:id", function (req, res) {
  res.setHeader("Content-Type", "application/json");
  let param_id;
  try {
    param_id = new mongo.ObjectID(req.params.id);
  } catch (e) {
    res.send(
      JSON.stringify({
        status: false,
        message: e.message,
      })
    );
  }

  db.collection("todo").updateOne(
    { _id: param_id },
    { $set: { status: "todo" } },
    function (err, result) {
      db.collection("todo").findOne({ _id: param_id }, function (err, result) {
        res.send(
          JSON.stringify({
            status: true,
            message: "TODO-ISSUE opened successfully",
            result,
          })
        );
        db.close();
      });
    }
  );
});

//CLOSE
app.get("/close/:id", function (req, res) {
  res.setHeader("Content-Type", "application/json");
  let param_id;
  try {
    param_id = new mongo.ObjectID(req.params.id);
  } catch (e) {
    res.send(
      JSON.stringify({
        status: false,
        message: e.message,
      })
    );
  }

  db.collection("todo").updateOne(
    { _id: param_id },
    { $set: { status: "finish" } },
    function (err, result) {
      db.collection("todo").findOne({ _id: param_id }, function (err, result) {
        res.send(
          JSON.stringify({
            status: true,
            message: "TODO-ISSUE closed successfully",
            result,
          })
        );
        db.close();
      });
    }
  );
});

// DELETE
app.get("/delete/:id", function (req, res) {
  res.setHeader("Content-Type", "application/json");
  let param_id;
  try {
    param_id = new mongo.ObjectID(req.params.id);
  } catch (e) {
    res.send(
      JSON.stringify({
        status: false,
        message: e.message,
      })
    );
  }

  db.collection("todo").deleteOne({ _id: param_id }, function (err, result) {
    res.send(
      JSON.stringify({
        status: true,
        message: "TODO-ISSUE deleted successfully",
        result,
      })
    );
    db.close();
  });
});

// START SERVER
app.listen(SERVER_PORT, function () {
  console.log(`Application running on port ${SERVER_PORT}!`);
});
